﻿from flask import Flask, request, render_template, redirect, url_for, session, send_from_directory, jsonify, abort
import os, time, configparser, pymysql.cursors, hashlib, ast, subprocess
from random import choice
from string import ascii_letters
from datetime import timedelta

DIR = os.path.abspath(os.curdir)
NAME = '/config.ini'
First_Start = not os.path.exists(DIR + NAME)

##DEF FUNCTION

def getConnectionConfig(host_s, user_s, password_s, db_s):
     
    # Вы можете изменить параметры соединения.
    connection = pymysql.connect(host=host_s,
                                 user=user_s,
                                 password=password_s,                             
                                 db=db_s,
                                 charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection

def getConfigSettigs():
	"""
	Подгружает необходимые конфиг параметры в глобальные переменные
	""" 
	global host_local, login_local, password_local, db_local, DIR, NAME
	path = DIR + NAME
	config = configparser.ConfigParser()
	config.read(path)
	host_local = config.get("Settings", "host")
	login_local = config.get("Settings", "login")
	password_local = config.get("Settings", "password")
	db_local = config.get("Settings", "db")

def getConnection():
	"""
		Connect DB in CONFIG FILE
	"""
	global host_local, login_local, password_local, db_local
	connection = pymysql.connect(host=host_local,
	                             user=login_local,
	                             password=password_local,                             
	                             db=db_local,
	                             charset='utf8',
	                             cursorclass=pymysql.cursors.DictCursor)
	return connection


def createConfig(login, password, host, db):
    """
    Create a config file
    """
    global DIR, NAME
    path = DIR + NAME
    config = configparser.ConfigParser()
    config.add_section("Settings")
    config.set("Settings", "login", login)
    config.set("Settings", "password", password)
    config.set("Settings", "host", host)
    config.set("Settings", "db", db)
    
    with open(path, "w") as config_file:
        config.write(config_file)

def us_profile():
	#check User from SESSION
	global session
	try:
		session['profile']
	except KeyError:
		return False
	return True

def get_name():
	#Function, getting name and Role{admin - 0, deffer - 1, attack - 2} Users
	global session
	return (session['profile']['name'], session['profile']['role'])

def create_slug(n):
	#Create Login-Slug in N symbols
	return ''.join(choice(ascii_letters + '1234567890' ) for i in range(n))

def execute_sql(a):
	#Предотвращение Sql ИНёекции
	return a.replace("'", "").replace(";","").replace("%", "").replace("-", "").replace("\"", "").replace("=","").replace("|", "")

def after_eavl(a):
	#{....}
	if a[0] == '{' and a[-1] == '}':
		return a
	elif a[0] == '{':
		return a+'}'
	elif a[-1] == '}':
		return '{'+a
	else:
		return '{'+a+'}'
## DEF END-FUNCTION

if not First_Start:
	#Есть Конфиг файл
	#CONFIG:
	getConfigSettigs()

app = Flask(__name__)

@app.route('/',  methods=['GET', 'POST'])
# if User in SESSION ----> redirected(/ADMIN)
def admin_submit():
	global First_Start
	if First_Start:
		return render_template('config.html')
	if us_profile():
		return redirect('/admin')
	return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
#this operetion LogIn User, if your login/slug virifecated, and, created session your information
# [ ID | NAME | Login(slug) | ROLE ]
# role = (admin, deffense, attack)
def login():
	if request.method == 'GET':
		return redirect("/")
	else:
		login = request.form.get('login')
		if not(login):
			return redirect('/')
		login = execute_sql(login)
		qr = "SELECT `name`, `role` FROM `users` WHERE login=%s" 
		conn = getConnection()
		cursor = conn.cursor()
		cursor.execute(qr, ( login ))
		#перемення, которая хранит Хук от действия Data
		Data = cursor.fetchone()
		conn.close()
		if not(Data):
			return "0"
		session['profile'] = {'name': Data['name'],'role': Data['role']}
		print("my@log >", session)
		return "1"

@app.route('/logout', methods=['GET', 'POST'])
#logout User, delete your Session from USER
def logout():
	session.pop('profile', None)
	return redirect('/')

@app.route('/admin')
#IF User Dont As Session, yours redirected
def admin():
	if not(us_profile()):
		return redirect('/')
	(name, role) = get_name()
	time_one = int(time.time()) % 86400
	conn = getConnection()
	cursor = conn.cursor()
	cursor.execute("SELECT COUNT(`id`) AS `count` FROM `flag` WHERE `done` = 1")
	count_flag_done = cursor.fetchone()['count']
	cursor.execute("SELECT COUNT(`id`) AS `count` FROM `flag`")
	count_flag_all = cursor.fetchone()['count']
	cursor.execute("SELECT COUNT(`id`) AS `count` FROM `users` WHERE `role` = 1 ")
	count_deffer = cursor.fetchone()['count']
	cursor.execute("SELECT COUNT(`id`) AS `count` FROM `users` WHERE `role` = 2 ")
	count_attack = cursor.fetchone()['count']
	cursor.execute("SELECT `attack_coins`, `defense_coins` FROM `bank` ORDER BY `id` DESC LIMIT 1")
	count_coins = cursor.fetchone()
	if not count_coins:
		count_coins = {'attack_coins':0, 'defense_coins':0}
	conn.close()
	return render_template('admin.html', name=name, role=role, time=time_one, flag=(count_flag_done, count_flag_all), count_user=(count_attack, count_deffer), count_coins=count_coins)

@app.route('/flag', methods=['GET', 'POST'])
def flag():
	#View Flag Attack or Admin
	if request.method == "GET":
		if not(us_profile()):
			return redirect('/')
		(name, role) = get_name()
		if role == 2:
			return render_template('flag.html', name=name, role=role)
		elif role == 0:
			conn = getConnection()
			cursor = conn.cursor()
			cursor.execute("SELECT `id_mac`, `done`, `mac`.`id_docker` FROM `flag` JOIN `mac` ON `mac`.`id` = `flag`.`id_mac` ORDER BY `id_mac`")
			#cursor.execute("SELECT `id_mac`, `done` FROM `flag`")
			Response = cursor.fetchall()
			conn.close()
			return render_template('flag.html', name=name, role=role, resp=Response)
		else:
			return redirect('/')
	else:
		(name, role) = get_name()
		if role == 2:
			flag = request.form.get('flag')
			if not(flag):
				abort(401)
			flag = execute_sql(flag)
			flag = hashlib.sha1(flag.encode()).hexdigest()
			conn = getConnection()
			cursor = conn.cursor()
			qr = "SELECT `id` FROM `flag` WHERE `hash` = %s AND `done` = %s"
			cursor.execute(qr,( flag, 0))
			id_flag = cursor.fetchone()
			if not(id_flag):
				conn.close()
				return 'error'
			else:
				id_flag = id_flag['id']
				qr = "UPDATE `flag` SET `done` = '1' WHERE `id` = %s;"
				cursor.execute(qr, ( id_flag ))
				conn.commit()
				qr = "UPDATE `bank` SET `defense_coins` = ( `defense_coins` - %s), `attack_coins` = (`attack_coins` + %s);"
				cursor.execute(qr, (200, 200))
				conn.commit()
				return '200'
		else:
			return redirect('/')

@app.route('/mon', methods=['GET', 'POST'])
def mon():
	if not(us_profile()):
		return redirect('/')
	(name, role) = get_name()
	if role != 0:
		return redirect('/')
	limit_time = 61
	if request.method == 'GET':
		conn = getConnection()
		cursor = conn.cursor()
		cursor.execute("SELECT COUNT(`id`) AS `count` FROM `mac`")
		count_machine = int(cursor.fetchone()['count'])
		Data = [i for i in range(count_machine)]
		return render_template('mon.html', name=name, role=role, resp = Data)
	else:
		#type request (monitoring / details)
		type_connect = request.form.get('type')
		if not type_connect:
			abort(401)
		type_connect = execute_sql(type_connect)
		#id_mac request
		id_mac = request.form.get('id')
		if not id_mac:
			abort(401)
		id_mac = execute_sql(id_mac)
		#type::
		if type_connect == 'monitor':
			conn = getConnection()
			cursor = conn.cursor()
			#Get Uptime Machine
			cursor.execute("SELECT `uptime` FROM `mac` WHERE `id` = %s", (id_mac))
			uptime = cursor.fetchone()['uptime']
			#Get last active
			qr_delay = "SELECT (NOW() - `real_time`) AS `delay` FROM `real_mon` WHERE `id_mac` = %s ORDER BY `real_time` DESC LIMIT 1"
			cursor.execute(qr_delay, ( id_mac ))
			delay = cursor.fetchone()
			if not delay:
				status = False #offline
			else:
				if int(delay['delay']) < 30:
					status = True #online
				else:
					status = False #offline
			#Create Json format Data
			AjaxData = {'cpu':[0 for k in range(limit_time)], 'ram':[0 for k in range(limit_time)], 'ok':'200', 'uptime':str(timedelta(seconds=int(uptime))), 'status':status}
			cursor.execute("SELECT `cpu`, `ram` FROM `real_mon` WHERE `id_mac` = %s ORDER BY `real_time` DESC LIMIT %s", (id_mac, limit_time))
			Response = cursor.fetchall()
			conn.close()
			real_limit = limit_time - 1
			for row in Response:
				AjaxData['cpu'][real_limit] = row['cpu']
				AjaxData['ram'][real_limit] = row['ram']
				real_limit-=1
			return jsonify(AjaxData)
		elif type_connect == 'details':
			conn = getConnection()
			cursor = conn.cursor()
			#MoreData
			cursor.execute("SELECT `processes` FROM `mac` WHERE `id` = %s", (id_mac))
			Response = cursor.fetchone()
			Response = list(map(ast.literal_eval ,map(after_eavl,Response['processes'].split('}{'))))
			print(Response)
			return render_template('details/table.html', resp=Response)
		else:
			abort(401)

@app.route('/profile', methods=['GET', 'POST'])
def profile():
	if not(us_profile()):
		return redirect('/')
	(name, role) = get_name()
	if role != 0:
		return redirect('/')
	#register USER in DB LOGIN
	conn = getConnection()
	cursor = conn.cursor()
	cursor.execute("SELECT * FROM `users` WHERE `role` IN (1, 2)")
	Response = cursor.fetchall()
	if request.method == "POST":
		id_del = request.form.get('id_del')
		if not(id_del):
			#REQUEST for Register User
			tname = request.form.get('tname')
			radioteam = request.form.get('radioteam')
			if radioteam == 'attack':
				upname = 'Team Attack'
				id_radioteam = 2
			elif radioteam == 'defense':
				upname = 'Team Defense'
				id_radioteam = 1
			login = request.form.get('login')
			if not(tname):
				tname = upname
			else:
				tname = execute_sql(tname)
			if not(login):
				login = create_slug(12)
			else:
				login = execute_sql(login)
			print('my@log>', tname, id_radioteam, login)
			#next code
			qr = "INSERT INTO `users` (`name`, `login`, `role`) VALUES (%s, %s, %s)"
			cursor.execute(qr, (tname, login, id_radioteam))
			conn.commit()
			return redirect('/profile')
		else:
			#REQUEST for Delete User
			id_del = execute_sql(id_del)
			print("my@log>", 'USER DELETE WHERE id =', id_del)
			cursor.execute("SELECT `role` FROM `users` WHERE `id`=%s", id_del)
			if cursor.fetchone()['role'] in (1, 2):
				cursor.execute("DELETE FROM `users` WHERE `id`=%s", id_del)
				conn.commit()
			return redirect('/profile')
	return render_template('profile.html', name=name, role=role, resp=Response)

@app.route('/settings', methods=['GET', 'POST'])
def settings():
	if not(us_profile):
		return redirect('/')
	(name, role) = get_name()
	if role != 0:
		return redirect('/')
	if request.method == 'GET':
		#GET
		conn = getConnection()
		cursor = conn.cursor()
		cursor.execute("SELECT (COUNT(`id`) > 0) AS `game` FROM `bank`")
		game_start = cursor.fetchone()
		if not game_start:
			abort(404)
		game_start = game_start['game']
		return render_template('settings.html', name=name, role=role, start=game_start)
	#POST
	type_connect = request.form.get('type')
	if not type_connect:
		abort(401)
	type_connect = execute_sql(type_connect)
	if type_connect == 'getable':
		#GET TABLE
		id_table = request.form.get('id')
		if not id_table:
			abort(401)
		id_table = execute_sql(id_table)
		return render_template('details/getable.html', id=id_table)
	elif type_connect == 'getconn':
		#GET CONNECTION
			login = request.form.get('login')
			password = request.form.get('password')
			host = request.form.get('host')
			db = request.form.get('db')
			if not(login and password and host and db):
				abort(404)
			(login, password, host, db) = map(execute_sql, (login, password, host, db))
			try:
				getConnectionConfig(host, login, password, db)
				return '200'
			except:
				return '0'
	else:
		abort(401)

@app.route('/config', methods=['GET', 'POST'])
def config():
	global First_Start
	if First_Start:
		if request.method == 'GET':
			abort(404)
		mode = request.form.get('mode')
		if mode == 'sql':
			login = request.form.get('login')
			password = request.form.get('password')
			host = request.form.get('host')
			db = request.form.get('db')
			if not(login and password and host and db):
				abort(404)
			(login, password, host, db) = map(execute_sql, (login, password, host, db))
			#SQL Обращения
			getConnectionConfig(host, login, password, db)
			return 'Done!'
		elif mode == 'admin':
			login = request.form.get('login')
			password = request.form.get('password')
			host = request.form.get('host')
			db = request.form.get('db')
			login_slug = request.form.get('login-slug')
			if not(login and password and host and db and login_slug):
				abort(404)
			(login, password, host, db, login_slug) = map(execute_sql, (login, password, host, db, login_slug))
			connection = getConnectionConfig(host, login, password, db)
			cursor = connection.cursor()
			"""
				CREATE TABLES
			"""
			sql = "CREATE TABLE IF NOT EXISTS `users` ( `id` INT(5) NOT NULL AUTO_INCREMENT , `name` VARCHAR(50) NOT NULL , `login` VARCHAR(50) NOT NULL , `role` INT(1) NOT NULL , PRIMARY KEY (`id`));"
			cursor.execute(sql)
			sql = "CREATE TABLE IF NOT EXISTS `flag` ( `id` INT(5) NOT NULL AUTO_INCREMENT , `id_mac` INT(5) NOT NULL , `hash` VARCHAR(255) NOT NULL , `done` BOOLEAN NOT NULL DEFAULT FALSE , PRIMARY KEY (`id`));"
			cursor.execute(sql)
			sql="CREATE TABLE IF NOT EXISTS `mac` ( `id` INT(5) NOT NULL AUTO_INCREMENT , `id_docker` VARCHAR(50) NOT NULL , `mach_users` TEXT NULL DEFAULT NULL,`uptime` FLOAT NULL DEFAULT NULL , `netconn` TEXT NULL DEFAULT NULL , `processes` TEXT NULL DEFAULT NULL , PRIMARY KEY (`id`));"
			cursor.execute(sql)
			sql = "CREATE TABLE IF NOT EXISTS `real_mon` ( `id` INT NOT NULL AUTO_INCREMENT , `id_mac` INT NOT NULL , `cpu` FLOAT NOT NULL , `ram` FLOAT NOT NULL , `real_time` TIMESTAMP NOT NULL , PRIMARY KEY (`id`));"
			cursor.execute(sql)
			sql="CREATE TABLE IF NOT EXISTS `bank` ( `id` INT(5) NOT NULL AUTO_INCREMENT , `attack_coins` INT NOT NULL DEFAULT '0' , `defense_coins` INT NOT NULL DEFAULT '0' , PRIMARY KEY (`id`));"
			cursor.execute(sql)
			sql = "SELECT `id` FROM `users` WHERE `name`='Administrator' LIMIT 1"
			cursor.execute(sql)
			if not cursor.fetchone():
				sql="INSERT INTO `users` (`id`, `name`, `login`, `role`) VALUES ('1', 'Administrator', %s, '0')"
				cursor.execute(sql, ( str(login_slug) ) )
			connection.commit()
			connection.close()
			"""
			Create Config
			"""
			createConfig(login, password, host, db)
			"""
				Nano Reboot server:
			"""
			getConfigSettigs()
			First_Start = not(First_Start)
			return '200'
	else:
		abort(404)
@app.route('/start', methods=['GET', 'POST'])
def start():
	global DIR
	if not(us_profile):
		return redirect('/')
	(name, role) = get_name()
	if role != 0:
		return redirect('/')
	#GET
	if request.method == 'GET':
		abort(404)
	#POST
	type_connect = request.form.get('type')
	if not type_connect:
		abort(401)
	if type_connect == 'start':
		#START DOCKER
		count = request.form.get('count')
		login = request.form.get('login')
		password = request.form.get('password')
		host = request.form.get('host')
		db = request.form.get('db')
		coins = request.form.get('coins')
		if not(count and login and password and host and db and coins):
			return '401'
		(count, login, password, host, db, coins) = map(execute_sql, (count, login, password, host, db, coins))
		try:
			if ((int(count) <= 0) or (int(coins) <= 0)):
				return '401'
		except:
			return '401'

		#Check yout DB
		(login, password, host, db) = map(execute_sql, (login, password, host, db))
		try:
			getConnectionConfig(host, login, password, db)
		except:
			return '401'
		conn = getConnection()
		cursor = conn.cursor()
		#Ready ti start?
		cursor.execute("SELECT (COUNT(`id`) > 0) AS `game` FROM `bank`")
		game_start = cursor.fetchone()
		if not game_start:
			abort(401)
		if game_start['game']:
			abort(401)
		sql = "INSERT INTO `bank`(`defense_coins`) VALUES (%s)"
		cursor.execute(sql, (coins))
		conn.commit()
		conn.close()
		#Start DOCKER!
		subprocess.Popen("sudo python3 docker_creator.py " + str(count), shell=True, cwd=DIR+'/vuln_mach')
		return '200'
	elif type_connect == 'stop':
		conn = getConnection()
		cursor = conn.cursor()
		sql=["DELETE FROM mac;","ALTER TABLE mac AUTO_INCREMENT = 0;","DELETE FROM real_mon;","ALTER TABLE real_mon AUTO_INCREMENT = 0;","DELETE FROM flag;","ALTER TABLE flag AUTO_INCREMENT = 0;","DELETE FROM `bank`;","ALTER TABLE `bank` AUTO_INCREMENT = 0;"]
		for qr in sql:
			cursor.execute(qr)
			conn.commit()
		conn.close()
		return redirect('/logout')
	else:
		abort(401)

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                          'favicon.ico',mimetype='image/vnd.microsoft.icon')

@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(hours=72)

if __name__ == '__main__':
    app.secret_key = """VHE1KML4nqE{asfK#Z4ZsR1rI5$HUC%lChC$h6R24i9a{5HH2@JNHkuOz?grSFQA6ja@I??uLIT{a7c?ysdFI4@u*f5MHe{dz2#
{6}K5utpQX?fPpk{ORnIPKvU7CUwB2~9O|y?QkvykR23nRaHFcnwNSO2Pdx~2B@rbCw~#D{0l#QmjnbNb~Vv$7do%Y8I7Q0lwoF
X$30vVt|H?a@9NQD8YEA3VGv~Hr?5AEP}~|Jm8m}3Zn}mOMCo%aivWQ#jNHfKp3C6%DJvbyDL#~mkFGXPxbVS4aXpeJtyy|77bv
e|4}dm#zBn*Yuoa~Qu9s70Ay9cU8s~j~Bow9{9EyiT%{yiPT%udC54vng}Aa%KtLa#T~OOcaOhbSsyJ7oW5Mfw{AN1@aUgM~JwG
w%fX~6mEuo80VOxTgkQelkMX@WGj4Xtd|rnDTB0q0DfgANxC2I0M~CXgQPBfO325N99w}zSZq}wzXs3W{t~}a~DaF7BF2KJR42a
n8ZWg@KopesNC@kOsWPTm#bSPDnB8I9MMa6p5kzna}~$1N|qwFdDAU#0yE$uyWHhKPA#{Tv42Kc$UDaTyrbF4LR0we$1be##nJ6
Y{lk?G*4$Mi{pPa3zD#4ElHqC1?i~3Y0OPGJdTY2MSMFF34~SXkI5bXLW@NtliBa*PG{M}ENw8X{uV@#O?hB2SWM#u?{1aMyNEQ
XQ|X@zt?TMmjH2~@iLG7Ro0MaJd1q0~26%e7C62xVKc@3~1EnGF44bP}KtD@AToxmki3$*}id{irN?MEDFnM$c1R4whzdrjTQ1I
MHSh%CzuSPR4fYA~en8eh6YpgJ1fPklKGGBhMhUIIy5xcvrpYv%L9aVb8cJI|$@%0fwMP@23xy858HT5Cu2F1N@Bcj@|L2veopJ
ht78z~~uhuWG7Mi~3Ea5smv%2#?{Z6hytLqqY9Z~i?I9Mg~bJ*2pjCSr~UkTvPGnJY0d~gNy3l{J9AY|30P7*?}b{bMe2FkdgeP"""

    app.debug = False
    app.run(host="0.0.0.0", port="5000")
    #app.run()