function createConfig(data1, data2){
 return {
    type: 'line',
    data:{
        labels:['60s' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'50' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'40' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'30' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'20' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'10' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'0'],
        datasets:[{
            label: 'CPU',
            backgroundColor: 'rgba(217, 83, 79, 0.27)',
            borderColor: 'rgb(217, 83, 79)',
            fill: true,
            data: data1,
        },
        {
            label: 'RAM',
            backgroundColor:'rgba(23, 162, 184, 0.2)',
            borderColor: 'rgb(23, 162, 184)',
            fill: true,
            data: data2,
        }],
        options: {
            legend: false,
            responsive: true,
            title: {
                display: false
            },
            scales: {
                xAxes: [{
                    display: false
                }],
                yAxes: [{
                    display: true,
                    beginAtZero: true,
                    min: 0,
                    max: 100,
                    stepSize: 50
                }]
            },
            animation: {
                duration: 0                    // general animation time
            },
            hover: {
                animationDuration: 0           // duration of animations when hovering an item
            },
            responsiveAnimationDuration: 0,
            },
    }
};
}
function update_ctx(ctx, id){
    $.ajax({
        type: "POST",
        url: "/mon",
        data: {'id':id, type:'monitor'},
        type: 'POST',
        success: function(data){
            if (data['ok'] == '200'){
                ctx.data.datasets[0].data = data['cpu'];
                ctx.data.datasets[1].data = data['ram'];
                $('#time_'+id).text(data['uptime'])
                ctx.update();
                if(data['status']){
                    //online
                    $('#table_'+id).removeClass('table-danger');
                    $('#status_'+id).text('Online');
                }
                else{
                    //offline
                    $('#table_'+id).addClass('table-danger');
                    $('#status_'+id).text('Offline');
                }
            }
        }
    });
}
function getdetails(id){
    $.ajax({
        type:"POST",
        url: "/mon",
        data:{'id':id, type:'details'},
        type:"POST",
        success: function(data){
            $('#on_'+id).hide();
            $('#off_'+id).hide();
            $('#details_'+id).html(data);
            $('#details_'+id).slideDown();
            $('#off_'+id).show();
        }
    })
}
function deldetails(id){
    $('#on_'+id).hide();
    $('#off_'+id).hide();
    $('#on_'+id).show();
    $('#details_'+id).slideUp();
    $('#details_'+id).html();
}
