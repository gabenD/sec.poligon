function generate(len) {
	var result       = '';
	var words        = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
	var max_position = words.length - 1;
		for( i = 0; i < len; ++i ) {
			position = Math.floor ( Math.random() * max_position );
			result = result + words.substring(position, position + 1);
		}
	return result;
}
function init()
{
	setInterval(tick, 1000);
}

function tick()
{
	tik = window.tik;
    tik++;
    tik = tik % (24 * 3600)
    sec = tik % 60
    min = Math.floor((tik%3600)/60)
    hour = Math.floor(tik/3600)
    time_one = "" + Math.floor(hour/10) + hour%10 + ":" + Math.floor(min/10) + min%10 + ":" + Math.floor(sec/10) + sec%10 ;
    document.getElementById("time_one").childNodes[0].nodeValue = time_one;
}

$(document).ready(function() {
	$('.p_servise').click(function(){
		$(this).closest('.info').children('.servise').toggle();
	});
	$('.error').click(function(){
		$(this).hide();
	});
	$('#generate-log').click(function(){
		$('#gen-login').val(generate(12));
	});
	$('.td_pass_show').click(function(){
		$(this).css({'display':'none'});
		$(this).closest('td').children('.pass').show();
		$(this).detach();
	});
	$('i.submit_form_del').click(function(){
		$(this).closest('form').submit();
	});
	/*Ajax Index Form*/
	$('#index_form').submit(function(){
		var text_l = $("#input_log").val();
		$(".error").text("");
		$("#input_log").removeClass('is-invalid');
		if (text_l){
			//Что-то есть
			$.ajax({
				type: "POST",
				url: "/login",
				data: {'login':text_l},
				type: 'POST',
				success: function(data){
					if(data == '1'){
						window.location.replace("/admin");
						$(".error").text("");
					}
					else{
						$(".error").text("Invalid login! Please try again.");
						$("#input_log").addClass('is-invalid');
					}
				}
			});
		}
		return false;
	});
	/*Flag Ajax Form*/
	$('#but-flag').click(function(){
		var flag = $("#val-flag").val();
		$('.error-flag').html('');
		if (flag){
			$.ajax({
				type: "POST",
				url: "/flag",
				data: {'flag':flag},
				type: 'POST',
				success: function(data){
					if(data == '200'){
						$('.error-flag').html(ok_alert()).delay(100).children('div').fadeIn(100);
					}
					else{
						$('.error-flag').html(wrong_alert()).delay(100).children('div').fadeIn(100);
					}
				}
			});
		} 
	});
	/*AddRowTable*/
	//1
	$('#add_row').click(function(){
		var count_row = document.getElementById('tbody-row').children.length + 1;
		$.ajax({
			type:'POST',
			url:'/settings',
			data:{'type':'getable', 'id':count_row},
			type:'POST',
			success: function(data){
				$('#tbody-row').append(data);
			}
		})
	});
	//2
	$('#remove_row').click(function(){
		$('#tbody-row tr').last().remove();
	});
	//3
	/*Start/Stop Docker*/
	$('#start_docker').click(function(){
		console.log()
		var	host = $('#host-sql').val();
    	var login = $('#login-sql').val();
    	var pass = $('#pass-sql').val();
    	var name = $('#name-sql').val();
    	var coins = $('#coins-start').val();
     
    if(host.length != 0 && login.length != 0 && pass.length != 0 && name.length != 0 && coins.length !=0){
    	checkParams();
    	if(window.nextStep_v){
    		var count = document.getElementById('tbody-row').children.length;
			$.ajax({
				type:'POST',
				url:'/start',
				data:{'type':'start','count':count,'login':login, 'password':pass, 'host':host, 'db':name, 'coins':coins},
				type:'POST',
				success: function(data){
					if (data == '200'){
						window.location.replace("/mon");
					}
					else{
						$('#sql-error').addClass('invalid-feedback').text('Coins/Machines should not be zero...');
					}
				}
			});
    	}
    }
	});
});
/*УБрать Потом*/
function objDump(object) {
    var out = "";
    if(object && typeof(object) == "object"){
        for (var i in object) {
            out += i + ": " + object[i] + "\n";
        }
    } else {
        out = object;
    }
        alert(out);
}

function ok_alert(){
	return '<div style="display:none;" class="alert alert-success" role="alert">Done!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
}
function wrong_alert(){
	return '<div style="display:none;" class="alert alert-danger" role="alert">Incorrect flag<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
}
window.nextStep_v = false;
function checkParams() {
	//Отсдеживание данных в Input
    var	host = $('#host-sql').val();
    var login = $('#login-sql').val();
    var pass = $('#pass-sql').val();
    var name = $('#name-sql').val();
     
    if(host.length != 0 && login.length != 0 && pass.length != 0 && name.length != 0) {
    	$.ajax({
    		type: "POST",
    		url: "/settings",
    		data: {'login':login, 'password':pass, 'host':host, 'db':name, 'type':'getconn'},
    		type: 'POST',
    		error: function(){
    			$('.error-sql').text('Invalid settings');
    		},
    		success: function(data){
    			$('.error-sql-s').text(data);
    			if (data == "200"){
    				$('#sql-error').addClass('valid-feedback').text("Done!");

    				$('#host-sql').addClass('is-valid');
    				$('#login-sql').addClass('is-valid');
    				$('#pass-sql').addClass('is-valid');
    				$('#name-sql').addClass('is-valid');

    				$('#host-sql').removeClass('is-invalid');
    				$('#login-sql').removeClass('is-invalid');
    				$('#pass-sql').removeClass('is-invalid');
    				$('#name-sql').removeClass('is-invalid');

    				$('#sql-error').removeClass('invalid-feedback');
    				window.nextStep_v = true;
    			}
    			else{
    				$('#sql-error').addClass('invalid-feedback').text('Invalid settings.');

    				$('#host-sql').removeClass('is-valid');
    				$('#login-sql').removeClass('is-valid');
    				$('#pass-sql').removeClass('is-valid');
    				$('#name-sql').removeClass('is-valid');

    				$('#host-sql').addClass('is-invalid');
    				$('#login-sql').addClass('is-invalid');
    				$('#pass-sql').addClass('is-invalid');
    				$('#name-sql').addClass('is-invalid');

    				$('#sql-error').removeClass('valid-feedback');

    				window.nextStep_v = false;
    			}

    		}
    	});
    }
}
