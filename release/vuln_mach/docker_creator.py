import os
import time
import argparse


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("machine_numbers", help="How many machines you want to create", type=int)
	args = parser.parse_args()
	i = 0
	while args.machine_numbers > i:
		os.system("docker run -itd -P 1a3178e8fce5")
		i += 1
		time.sleep(2)

if __name__ == '__main__':
    main()
